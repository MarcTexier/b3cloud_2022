# TP2 : Ansible

# 0. Setup

# I. Init repo

# II. Un dépôt Ansible rangé

## 1. Structure du dépôt : inventaires

➜ **Dans votre répertoire de travail Ansible...**

- créez un répertoire `inventories/`
- créez un répertoire `inventories/vagrant_lab/`
- déplacez le fichier `hosts.ini` dans `inventories/vagrant_lab/hosts.ini`
- assurez vous que pouvez toujours déployer correctement avec une commande `ansible-playbook`

```
 tp2-ansible git:(master) ✗ ansible-playbook -i Ansible/inventories/vagrant_lab/hosts.ini Ansible/test.yml

PLAY [Install vim] *********************************************************************************************************************************************

TASK [Gathering Facts] *****************************************************************************************************************************************
The authenticity of host 'node2.tp2.cloud (192.168.56.5)' can't be established.
ECDSA key fingerprint is SHA256:NS8ww4SMoWGIODdUNPWy2Vwt9bcakE9fL7zk2JGlzPbw.
Are you sure you want to continue connecting (yes/no/[fingerprint])? yes
The authenticity of host 'node1.tp2.cloud (192.168.56.4)' can't be established.
ECDSA key fingerprint is SHA256:FP/tYR0hUuiE85XpUHkiNv0HXC2XHJwYu+9bcakcsYwM.
Are you sure you want to continue connecting (yes/no/[fingerprint])? yes
ok: [node2.tp2.cloud]
ok: [node1.tp2.cloud]

TASK [apt-get update] ******************************************************************************************************************************************
ok: [node1.tp2.cloud]
ok: [node2.tp2.cloud]

TASK [install vim] *********************************************************************************************************************************************
ok: [node1.tp2.cloud]
ok: [node2.tp2.cloud]

PLAY RECAP *****************************************************************************************************************************************************
node1.tp2.cloud            : ok=3    changed=0    unreachable=0    failed=0    skipped=0    rescued=0    ignored=0   
node2.tp2.cloud            : ok=3    changed=0    unreachable=0    failed=0    skipped=0    rescued=0    ignored=0   

```

## 2. Structure du dépôt : rôles

## 3. Structure du dépôt : variables d'inventaire


```
Ansible git:(master) ✗ ansible-playbook -i inventories/vagrant_lab/hosts.ini playbooks/main.yml


PLAY [ynov] ****************************************************************************************************************************************************

TASK [Gathering Facts] *****************************************************************************************************************************************
The authenticity of host 'node2.tp2.cloud (192.168.56.5)' can't be established.
ECDSA key fingerprint is SHA256:NS8ww4SMoWGIODdUNPWy2Vwt9bcakE9fL7zk2JGlzPbw.
Are you sure you want to continue connecting (yes/no/[fingerprint])? yes
The authenticity of host 'node1.tp2.cloud (192.168.56.4)' can't be established.
ECDSA key fingerprint is SHA256:FP/tYR0hUuiE85XpUHkiNv0HXC2XHJwYu+9bcakusYwM.
Are you sure you want to continue connecting (yes/no/[fingerprint])? ok: [node2.tp2.cloud]
yes
ok: [node1.tp2.cloud]

TASK [common : Install common packages] ************************************************************************************************************************
ok: [node2.tp2.cloud] => (item=vim)
ok: [node1.tp2.cloud] => (item=vim)
ok: [node2.tp2.cloud] => (item=git)
ok: [node1.tp2.cloud] => (item=git)
ok: [node1.tp2.cloud] => (item=rsync)

PLAY RECAP *****************************************************************************************************************************************************
node1.tp2.cloud            : ok=2    changed=0    unreachable=0    failed=0    skipped=0    rescued=0    ignored=0   
node2.tp2.cloud            : ok=2    changed=0    unreachable=0    failed=0    skipped=0    rescued=0    ignored=0   
```


```
Ansible git:(master) ✗ ansible-playbook -i inventories/vagrant_lab/hosts.ini playbooks/main.yml


PLAY [ynov] ****************************************************************************************************************************************************

TASK [Gathering Facts] *****************************************************************************************************************************************
ok: [node1.tp2.cloud]
ok: [node2.tp2.cloud]

TASK [common : Install common packages] ************************************************************************************************************************
ok: [node1.tp2.cloud] => (item=vim)
ok: [node2.tp2.cloud] => (item=vim)
ok: [node1.tp2.cloud] => (item=git)
ok: [node2.tp2.cloud] => (item=git)
ok: [node1.tp2.cloud] => (item=rsync)

TASK [common : Create a users attached to ynov group] **********************************************************************************************************
changed: [node2.tp2.cloud] => (item=le_nain)
changed: [node1.tp2.cloud] => (item=le_nain)
changed: [node1.tp2.cloud] => (item=l_elfe)
changed: [node2.tp2.cloud] => (item=l_elfe)
changed: [node2.tp2.cloud] => (item=le_ranger)
[WARNING]: The input password appears not to have been hashed. The 'password' argument must be encrypted for this module to work properly.
changed: [node1.tp2.cloud] => (item=le_ranger)

PLAY RECAP *****************************************************************************************************************************************************
node1.tp2.cloud            : ok=3    changed=1    unreachable=0    failed=0    skipped=0    rescued=0    ignored=0   
node2.tp2.cloud            : ok=3    changed=1    unreachable=0    failed=0    skipped=0    rescued=0    ignored=0   
```

```
vagrant@node1:~$ cat /etc/passwd | grep 'le_nain\|l_elfe\|le_ranger'
le_nain:x:1004:1006::/home/le_nain:/bin/sh
l_elfe:x:1005:1007::/home/l_elfe:/bin/sh
le_ranger:x:1006:1008::/home/le_ranger:/bin/sh
```



## 4. Structure du dépôt : rôle avancé

```
Ansible git:(master) ✗ ansible-playbook -i inventories/vagrant_lab/hosts.ini playbooks/main.yml 


PLAY [ynov] ****************************************************************************************************************************************************

TASK [Gathering Facts] *****************************************************************************************************************************************
ok: [node2.tp2.cloud]
ok: [node1.tp2.cloud]

TASK [common : Install common packages] ************************************************************************************************************************
ok: [node2.tp2.cloud] => (item=vim)
ok: [node1.tp2.cloud] => (item=vim)
ok: [node2.tp2.cloud] => (item=git)
ok: [node1.tp2.cloud] => (item=git)
ok: [node1.tp2.cloud] => (item=rsync)

TASK [common : Create a users attached to ynov group] **********************************************************************************************************
ok: [node2.tp2.cloud] => (item=le_nain)
ok: [node1.tp2.cloud] => (item=le_nain)
ok: [node2.tp2.cloud] => (item=l_elfe)
ok: [node1.tp2.cloud] => (item=l_elfe)
ok: [node2.tp2.cloud] => (item=le_ranger)
ok: [node1.tp2.cloud] => (item=le_ranger)

TASK [nginx : Install NGINX] ***********************************************************************************************************************************
ok: [node2.tp2.cloud]
ok: [node1.tp2.cloud]

TASK [nginx : Main NGINX config file] **************************************************************************************************************************
ok: [node1.tp2.cloud]
ok: [node2.tp2.cloud]

TASK [nginx : Create webroot] **********************************************************************************************************************************
ok: [node2.tp2.cloud]
ok: [node1.tp2.cloud]

TASK [nginx : Create index] ************************************************************************************************************************************
ok: [node2.tp2.cloud]
ok: [node1.tp2.cloud]

TASK [nginx : NGINX Virtual Host] ******************************************************************************************************************************
changed: [node1.tp2.cloud]
changed: [node2.tp2.cloud]

PLAY RECAP *****************************************************************************************************************************************************
node1.tp2.cloud            : ok=8    changed=1    unreachable=0    failed=0    skipped=0    rescued=0    ignored=0   
node2.tp2.cloud            : ok=8    changed=1    unreachable=0    failed=0    skipped=0    rescued=0    ignored=0   

```



## 5. Gérer la suppression


```
tp2-ansible git:(master) ✗ cat Ansible/roles/nginx/tasks/add_vhosts.yml 
- name: Create webroot
  become: yes
  file:
    path: "{{ add_vhosts['nginx_webroot'] }}"
    state: directory

- name: Create index
  become: yes
  copy:
    dest: "{{ add_vhosts['nginx_webroot'] }}/index.html"
    content: "{{ add_vhosts['nginx_index_content'] }}"

- name: NGINX Virtual Host
  become: yes
  template:
    src: vhost.conf.j2
    dest: /etc/nginx/conf.d/{{ add_vhosts['nginx_servername'] }}.conf
```

```
tp2-ansible git:(master) ✗ cat Ansible/roles/nginx/tasks/remove_vhosts.yml 
- name: Remove index
  become: yes
  file:
    path: "{{ remove_vhosts['nginx_webroot'] }}/index.html"
    state: absent

- name: Remove NGINX Virtual Host
  become: yes
  file: 
    path: /etc/nginx/conf.d/{{ remove_vhosts['nginx_servername'] }}.conf
    state: absent
```

* testez que vous pouvez facilement ajouter ou supprimer des Virtual Hosts depuis le fichier `host_vars` d'une machine donnée :

```yaml
➜  tp2-ansible git:(master) ✗ cat Ansible/inventories/vagrant_lab/host_vars/node1.tp2.cloud.yml 
common_packages:
  - vim
  - git
  - rsync

add_vhosts:
  nginx_servername: testnode3
  nginx_port: 8080
  nginx_webroot: /var/www/html/testnode3
  nginx_index_content: "<h1>teeeeeestnode3</h1>"

remove_vhosts:
  nginx_servername: testnode3
  nginx_port: 8080
  nginx_webroot: /var/www/html/testnode3
  nginx_index_content: "<h1>teeeeeestnode3</h1>"%
```

```yaml
➜  tp2-ansible git:(master) ✗ cat Ansible/inventories/vagrant_lab/host_vars/node2.tp2.cloud.yml
common_packages:
  - vim
  - git
  - rsync

add_vhosts:
  nginx_servername: testnode4
  nginx_port: 8080
  nginx_webroot: /var/www/html/testnode4
  nginx_index_content: "<h1>teeeeeestnode4</h1>"

remove_vhosts:
  nginx_servername:
  nginx_port:
  nginx_webroot: 
  nginx_index_content: %  
```

```bash
➜  Ansible git:(master) ✗ ansible-playbook -i inventories/vagrant_lab/hosts.ini playbooks/main.yml 

PLAY [ynov] ***************************************************************************************************************************************************************************

TASK [Gathering Facts] ****************************************************************************************************************************************************************
ok: [node1.tp2.cloud]
ok: [node2.tp2.cloud]

TASK [common : Install common packages] ***********************************************************************************************************************************************
ok: [node1.tp2.cloud] => (item=vim)
ok: [node2.tp2.cloud] => (item=vim)
ok: [node1.tp2.cloud] => (item=git)
ok: [node2.tp2.cloud] => (item=git)
ok: [node1.tp2.cloud] => (item=rsync)
ok: [node2.tp2.cloud] => (item=rsync)

TASK [common : Create a users attached to ynov group] *********************************************************************************************************************************
ok: [node1.tp2.cloud] => (item=le_nain)
ok: [node2.tp2.cloud] => (item=le_nain)
ok: [node1.tp2.cloud] => (item=l_elfe)
ok: [node2.tp2.cloud] => (item=l_elfe)
ok: [node2.tp2.cloud] => (item=le_ranger)
ok: [node1.tp2.cloud] => (item=le_ranger)

TASK [nginx : Install NGINX] **********************************************************************************************************************************************************
ok: [node1.tp2.cloud]
ok: [node2.tp2.cloud]

TASK [nginx : Main NGINX config file] *************************************************************************************************************************************************
ok: [node2.tp2.cloud]
ok: [node1.tp2.cloud]

TASK [nginx : Create webroot] *********************************************************************************************************************************************************
ok: [node2.tp2.cloud]
ok: [node1.tp2.cloud]

TASK [nginx : Create index] ***********************************************************************************************************************************************************
ok: [node2.tp2.cloud]
changed: [node1.tp2.cloud]

TASK [nginx : NGINX Virtual Host] *****************************************************************************************************************************************************
ok: [node2.tp2.cloud]
changed: [node1.tp2.cloud]

TASK [nginx : Remove index] ***********************************************************************************************************************************************************
changed: [node1.tp2.cloud]
ok: [node2.tp2.cloud]

TASK [nginx : Remove NGINX Virtual Host] **********************************************************************************************************************************************
changed: [node1.tp2.cloud]
ok: [node2.tp2.cloud]

PLAY RECAP ****************************************************************************************************************************************************************************
node1.tp2.cloud            : ok=10   changed=4    unreachable=0    failed=0    skipped=0    rescued=0    ignored=0   
node2.tp2.cloud            : ok=10   changed=0    unreachable=0    failed=0    skipped=0    rescued=0    ignored=0   
```



# III. Repeat

## 1. NGINX


```
Ansible git:(master) ✗ cat inventories/vagrant_lab/host_vars/node1.tp2.cloud.yml 
common_packages:
  - vim
  - git
  - rsync

add_vhosts:
  - test1:
    nginx_servername: testnode33
    nginx_port: 8080
    nginx_webroot: /var/www/html/testnode33
    nginx_index_content: "<h1>teeeeeestnode3</h1>"
  - test2:
    nginx_servername: test23
    nginx_port: 8081
    nginx_webroot: /var/www/html/test23
    nginx_index_content: "<h1>teeeeeest 2</h1>"
  - test3:
    nginx_servername: test33
    nginx_port: 8082
    nginx_webroot: /var/www/html/test33
    nginx_index_content: "<h1>teeeeeest 3</h1>"

remove_vhosts:
  - test3:
    nginx_servername: test33
    nginx_port: 8082
    nginx_webroot: /var/www/html/test33
    nginx_index_content: "<h1>teeeeeest 3</h1>"% 
```

```
Ansible git:(master) ✗ cat inventories/vagrant_lab/host_vars/node2.tp2.cloud.yml
common_packages:
  - vim
  - git
  - rsync

add_vhosts:
  - test1:
    nginx_servername: testnode334
    nginx_port: 8080
    nginx_webroot: /var/www/html/testnode334
    nginx_index_content: "<h1>teeeeeestnode3</h1>"
  - test2:
    nginx_servername: test234
    nginx_port: 8081
    nginx_webroot: /var/www/html/test234
    nginx_index_content: "<h1>teeeeeest 2</h1>"
  - test3:
    nginx_servername: test334
    nginx_port: 8082
    nginx_webroot: /var/www/html/test334
    nginx_index_content: "<h1>teeeeeest 3</h1>"

remove_vhosts:
  - test2:
    nginx_servername: test234
    nginx_port: 8081
    nginx_webroot: /var/www/html/test234
    nginx_index_content: "<h1>teeeeeest 2</h1>"%   
```

```
Ansible git:(master) ✗ cat roles/nginx/tasks/remove_vhosts.yml 
- name: Remove webroot
  become: yes
  file:
    path: "{{ item.nginx_webroot }}"
    state: absent
  with_items: '{{ remove_vhosts }}'

- name: Remove index
  become: yes
  file:
    path: "{{ item.nginx_webroot }}/index.html"
    state: absent
  with_items: '{{ remove_vhosts }}'

- name: Remove NGINX Virtual Host
  become: yes
  file: 
    path: /etc/nginx/conf.d/{{ item.nginx_servername }}.conf
    state: absent
  with_items: '{{ remove_vhosts }}'
```

```
Ansible git:(master) ✗ cat roles/nginx/tasks/add_vhosts.yml 
- name: Create webroot
  become: yes
  file:
    path: "{{ item.nginx_webroot }}"
    state: directory
  with_items: '{{ add_vhosts }}'

- name: Create index
  become: yes
  copy:
    dest: "{{ item.nginx_webroot }}/index.html"
    content: "{{ item.nginx_index_content }}"
  with_items: '{{ add_vhosts }}'

- name: NGINX Virtual Host
  become: yes
  template:
    src: vhost.conf.j2
    dest: /etc/nginx/conf.d/{{ item.nginx_servername }}.conf
  with_items: '{{ add_vhosts }}'
```

Ajoutez une mécanique de `handlers/

```
Ansible git:(master) cat roles/nginx/handlers/main.yml 
- name: Restart nginx
  become: yes
  service:
    name: nginx
    state: restarted%  
```

```
Ansible git:(master) cat roles/nginx/tasks/config.yml 
- name : Main NGINX config file
  become: yes
  copy:
    src: nginx.conf # pas besoin de préciser de path, il sait qu'il doit chercher dans le dossier files/
    dest: /etc/nginx/nginx.conf
  notify: Restart nginx
```

## 2. Common

```
tp2-ansible git:(master) ✗ cat Ansible/roles/common/tasks/users.yml 
- name: Create a users attached to ynov group
  become: yes
  ansible.builtin.user:
    name: "{{ item.name }}"
    password: "{{ item.password }}"
    state: present
    shell: /bin/bash       # Defaults to /bin/bash
    system: no             # Defaults to no
    createhome: yes        # Defaults to yes
    home: "{{ item.home }}"
    groups: "{{ item.groups }}"
  with_items: "{{ users }}" 
- name: Add public key
  become: yes
  ansible.posix.authorized_key:
    user: "{{ item.name }}"
    key: "{{ item.key }}"
    state: present
  with_items: "{{ users }}" # ceci permet de boucler sur la liste common_packages%  
```

```
tp2-ansible git:(master) ✗ cat Ansible/inventories/vagrant_lab/group_vars/ynov.yml 
users:
  - user1:
    name: le_nain
    password: "{{ 'password' | password_hash('sha512', 'password1') }}"
    home: /home/le_nain
    key: https://gitlab.com/LeMarquiis.keys
    groups: # Empty by default, here we give it some groups
      - sudo
      - admin
  - user2:
    name: l_elfe
    password: "{{ 'password' | password_hash('sha512', 'password2') }}"
    home: /home/l_elfe
    key: https://gitlab.com/LeMarquiis.keys
    groups: # Empty by default, here we give it some groups
      - sudo
      - admin
  - user3:
    name: le_ranger
    password: "{{ 'password' | password_hash('sha512', 'password3') }}"
    home: /home/le_ranger
    key: https://gitlab.com/LeMarquiis.keys
    groups: # Empty by default, here we give it some groups
      - sudo
      - admin
```



## 3. Dynamic loadbalancer

```
Ansible git:(master) ✗ cat roles/rproxy/tasks/config.yml 
- name : Main NGINX config file
  copy:
    src: nginx.conf # pas besoin de préciser de path, il sait qu'il doit chercher dans le dossier files/
    dest: /etc/nginx/nginx.conf
  notify: Restart nginx
  
- name : rproxy NGINX config file
  template:
    src:  proxy.conf.j2
    dest: /etc/nginx/proxy.conf
  notify: Restart nginx
```
```
Ansible git:(master) ✗ cat roles/rproxy/tasks/main.yml  
- name: Install NGINX
  import_tasks: install.yml

- name: Configure NGINX
  import_tasks: config.yml                                                                                                                                                    
```
```
➜  Ansible git:(master) ✗ cat roles/rproxy/templates/proxy.conf.j2 
upstream super_application {
    {% for host in groups['ynov'] %}
        server {{ host }}
    {% endfor %}
}

server {
    server_name anyway.com;

    location / {
        proxy_pass http://super_application;
        proxy_set_header    Host $host;
    }
}
