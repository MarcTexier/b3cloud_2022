terraform {
  required_providers {
    azurerm = {
      source  = "hashicorp/azurerm"
      version = ">=3.0.0"
    }
  }
}

provider "azurerm" {
  features {}
}

resource "azurerm_resource_group" "rg-b3-cloud" {
  name     = "b3-cloud"
  location = "eastus"
}

resource "azurerm_virtual_network" "vn-b3-cloud" {
  name                = "b3-cloud"
  address_space       = ["10.0.0.0/16"]
  location            = azurerm_resource_group.rg-b3-cloud.location
  resource_group_name = azurerm_resource_group.rg-b3-cloud.name
}

resource "azurerm_subnet" "s-b3-cloud" {
  name                 = "internal"
  resource_group_name  = azurerm_resource_group.rg-b3-cloud.name
  virtual_network_name = azurerm_virtual_network.vn-b3-cloud.name
  address_prefixes     = ["10.0.2.0/24"]
}


resource "azurerm_network_interface" "nic-b3-vm1" {
  name                = "example1-nic"
  location            = azurerm_resource_group.rg-b3-cloud.location
  resource_group_name = azurerm_resource_group.rg-b3-cloud.name

  ip_configuration {
    name                          = "internal"
    subnet_id                     = azurerm_subnet.s-b3-cloud.id
    private_ip_address_allocation = "Dynamic"
    public_ip_address_id          = azurerm_public_ip.b3example.id
  }
}

resource "azurerm_network_interface" "nic-b3-vm2" {
  name                = "example2-nic"
  location            = azurerm_resource_group.rg-b3-cloud.location
  resource_group_name = azurerm_resource_group.rg-b3-cloud.name

  ip_configuration {
    name                          = "internal"
    subnet_id                     = azurerm_subnet.s-b3-cloud.id
    private_ip_address_allocation = "Dynamic"
  }
}

resource "azurerm_public_ip" "b3example" {
  name                    = "testb3"
  location                = azurerm_resource_group.rg-b3-cloud.location
  resource_group_name     = azurerm_resource_group.rg-b3-cloud.name
  allocation_method       = "Dynamic"
  idle_timeout_in_minutes = 30
}

resource "azurerm_linux_virtual_machine" "vm-b3-vm1" {
  name                = "b3-vm1"
  resource_group_name = azurerm_resource_group.rg-b3-cloud.name
  location            = azurerm_resource_group.rg-b3-cloud.location
  size                = "Standard_B1s"
  admin_username      = "marc"
  network_interface_ids = [
    azurerm_network_interface.nic-b3-vm1.id,
  ]

  admin_ssh_key {
    username   = "marc"
    public_key = file("C:/Users/33623/ynov.pub")
  }

  os_disk {
    caching              = "ReadWrite"
    storage_account_type = "Standard_LRS"
  }

  source_image_reference {
    publisher = "Canonical"
    offer     = "UbuntuServer"
    sku       = "18.04-LTS"
    version   = "latest"
  }
}

#resource "azurerm_linux_virtual_machine" "vm-b3-vm2" {
#  name                = "b3-vm2"
#  resource_group_name = azurerm_resource_group.rg-b3-cloud.name
#  location            = azurerm_resource_group.rg-b3-cloud.location
#  size                = "Standard_B1s"
#  admin_username      = "marc"
#  network_interface_ids = [
#    azurerm_network_interface.nic-b3-vm2.id,
#  ]

#  admin_ssh_key {
#    username   = "marc"
#    public_key = file("C:/Users/33623/.ssh/id_rsa.pub")
#  }

#  os_disk {
#    caching              = "ReadWrite"
#    storage_account_type = "Standard_LRS"
#  }

#  source_image_reference {
#    publisher = "OpenLogic"
#    offer     = "CentOs"
#    sku       = "7.5"
#    version   = "latest"
#  }
#}